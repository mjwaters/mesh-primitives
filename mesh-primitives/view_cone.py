from plot_mesh_with_mpl import plot_mesh_with_mpl
from fileio import write_face_vertex_mesh_to_stl
import matplotlib.pyplot as plt
from cone import cone_primitive


# default
mesh = cone_primitive(length =4)
ax = plot_mesh_with_mpl(mesh)
write_face_vertex_mesh_to_stl(
    'cone_0.stl',
    faces=mesh.faces,
    vertices=mesh.verts )



# setting mesh refinement via generic integer
mesh = cone_primitive(length =4, fineness=3)
mesh.translate([2,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'green')


# setting side ie polygons around the circumference
mesh = cone_primitive(length =4, sides=7)
mesh.translate([4,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'purple')


#########
ax.set_xlim3d((-1,5))
ax.set_ylim3d((-3,3))
ax.set_zlim3d((-1,5))
plt.show()
