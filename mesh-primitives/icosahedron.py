from mesh import Mesh
import numpy as np

def icosahedron(radius = 1):
    '''radius is circumradius'''

    # a hacky trick using the modified cylinder code and an icosahedron in cylindircal coordinates
    nsides = 5
    dtheta = 2*np.pi/nsides

    xyrad = 2/np.sqrt(5) #np.sin(arctan(2))
    dz = 1/np.sqrt(5) #np.cos(arctan(2))

    verts = []
    faces = []
    for ns in range(nsides):
        verts.append([xyrad*np.cos(      ns*dtheta), xyrad*np.sin(      ns*dtheta), -dz])
        verts.append([xyrad*np.cos((ns+1/2)*dtheta), xyrad*np.sin((ns+1/2)*dtheta),  dz])
        faces.append([
            2*ns,
            2*ns+1,
            (2*ns-1)%(2*nsides)])
        faces.append([
            2*ns+1,
            2*ns, 
            (2*ns+2)%(2*nsides)])

    verts.append([0, 0, -1]) # index =  2*nsides
    verts.append([0, 0,  1]) # index =  2*nsides+1
    for ns in range(nsides): # bottom cap triangles before top cap
        faces.append([2*ns,     2*nsides, (2*ns+2)%(2*nsides)])
    for ns in range(nsides):
        faces.append([2*ns+1, 2*nsides+1, (2*ns-1)%(2*nsides)])
    return Mesh(verts*radius, faces)
