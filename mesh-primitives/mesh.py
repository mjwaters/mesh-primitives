import numpy as np

class Mesh:

    def __init__(self, verts, faces):
        self.verts = np.array(verts)
        self.faces = np.array(faces)

    def translate(self, vector):
        self.verts+=np.array(vector)
