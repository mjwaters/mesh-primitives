from plot_mesh_with_mpl import plot_mesh_with_mpl
from fileio import write_face_vertex_mesh_to_stl
import matplotlib.pyplot as plt

from icosahedron import icosahedron

mesh = icosahedron()

import numpy as np
print(np.linalg.norm(mesh.verts,axis=1))

write_face_vertex_mesh_to_stl(
    'icosahedron_0.stl',
    faces=mesh.faces,
    vertices=mesh.verts )

plot_mesh_with_mpl(mesh, show=True)


plt.show()
