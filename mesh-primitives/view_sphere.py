from plot_mesh_with_mpl import plot_mesh_with_mpl
from fileio import write_face_vertex_mesh_to_stl
import matplotlib.pyplot as plt
from sphere import sphere_primitive_polarmesh


# default
mesh = sphere_primitive_polarmesh(radius=1)
ax = plot_mesh_with_mpl(mesh)
write_face_vertex_mesh_to_stl(
    'sphere_0.stl',
    faces=mesh.faces,
    vertices=mesh.verts )


# setting mesh refinement via generic integer
mesh = sphere_primitive_polarmesh(radius=1, fineness=3)
mesh.translate([2,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'green')


# setting side ie polygons around the circumference
mesh = sphere_primitive_polarmesh(radius=1, sides=10)
mesh.translate([4,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'purple')

# removes the half twist to look more like a basic rectangular
#grid of theta/phi points
mesh = sphere_primitive_polarmesh(radius=1, use_half_step_twist=False)
mesh.translate([6,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'teal')


#########
ax.set_xlim3d((-1,7))
ax.set_ylim3d((-4,4))
ax.set_zlim3d((-4,4))
plt.show()
