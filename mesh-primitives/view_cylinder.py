from plot_mesh_with_mpl import plot_mesh_with_mpl
from fileio import write_face_vertex_mesh_to_stl
import matplotlib.pyplot as plt
from cylinder import cylinder_primitive


# default
mesh = cylinder_primitive(length =4)
ax = plot_mesh_with_mpl(mesh)
write_face_vertex_mesh_to_stl(
    'cylinder_0.stl',
    faces=mesh.faces,
    vertices=mesh.verts )


# setting mesh refinement via generic integer
mesh = cylinder_primitive(length =4, fineness=3)
mesh.translate([2,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'green')


# setting side ie polygons around the circumference
mesh = cylinder_primitive(length =4, sides=7)
mesh.translate([4,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'purple')

# removes the half twist to look more like a basic rectangular
#grid of theta/phi points
mesh = cylinder_primitive(length =4, fineness=0, use_half_step_twist=False)
mesh.translate([6,0,0])
plot_mesh_with_mpl(mesh, ax=ax, color = 'teal')
write_face_vertex_mesh_to_stl(
    'cylinder_3.stl',
    faces=mesh.faces,
    vertices=mesh.verts )



#########
ax.set_xlim3d((-1,7))
ax.set_ylim3d((-4,4))
ax.set_zlim3d((-2,6))
plt.show()
