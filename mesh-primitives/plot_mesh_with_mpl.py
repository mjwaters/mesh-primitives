import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


def plot_mesh_with_mpl(mesh,
                    ax=None,
                    figsize=(4, 3),
                    use_tight_axes_lims = False,
                    show=False,
                    color = None):
    if ax is None:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, projection='3d')


    # Fancy indexing: `verts[faces]` to generate a collection of triangles
    tri =  mesh.verts[mesh.faces]
    mesh_P3DC = Poly3DCollection(tri)
    mesh_P3DC.set_edgecolor('k')
    if color is not None:
        mesh_P3DC.set_facecolor(color)
    ax.add_collection3d(mesh_P3DC)


    ax.set_xlabel("x-axis")
    ax.set_ylabel("y-axis")
    ax.set_zlabel("z-axis")

    if use_tight_axes_lims:
        xmax=tri[:,:,0].max()
        xmin=tri[:,:,0].min()
        ymax=tri[:,:,1].max()
        ymin=tri[:,:,1].min()
        zmax=tri[:,:,2].max()
        zmin=tri[:,:,2].min()


        ax.set_xlim((xmin,xmax))
        ax.set_ylim((ymin,ymax))  # b = 10
        ax.set_zlim((zmin,zmax))  # c = 16
        fig = ax.get_figure()
        fig.tight_layout()

    if show:
        #fig = ax.get_figure()
        #fig.show()
        plt.show()

    return ax
