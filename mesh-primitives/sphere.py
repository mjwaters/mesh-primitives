from mesh import Mesh
import numpy as np


def sphere_primitive_polarmesh(radius=1, fineness = 2, sides = None, use_half_step_twist=True):
    '''fineness should give be used universal refinement of primatives with the mesh'''
    if sides is None:
        assert type(fineness)==int
        assert fineness >= 0
        nphi = 3 * 2**(fineness)
    else:
        assert sides >= 3
        nphi = sides

    ntheta = int(max(np.round(nphi/2),2))
    #print(ntheta,nphi)

    dtheta =   np.pi/ntheta #angle from z axis
    dphi   = 2*np.pi/nphi

    def vi(theta_index,phi_index):
        '''returns vert_index from the flattened set of points'''
        phi_i = phi_index%nphi
        vert_index = nphi*(theta_index-1)+phi_i
        return vert_index

    verts = []
    faces = []
    hs=0
    for theta_index in range(1,ntheta):
        for phi_index in range(nphi):
            verts.append([  radius*np.cos((phi_index+hs)*dphi)*np.sin(theta_index*dtheta),
                            radius*np.sin((phi_index+hs)*dphi)*np.sin(theta_index*dtheta),
                            radius*np.cos(theta_index*dtheta)])
        if use_half_step_twist:
            hs+= 1/2  #hs => half step phi shift for aesthetics

    if ntheta >= 3:
        for theta_index in range(1,ntheta-1):
            for phi_index in range(nphi):
                faces.append([
                    vi(theta_index,  phi_index+1),
                    vi(theta_index, phi_index),
                    vi(theta_index+1, phi_index)
                    ])
                faces.append([
                    vi(theta_index+1,phi_index+1),
                    vi(theta_index, phi_index+1),
                    vi(theta_index+1, phi_index)
                    ])

    if True:
        verts.append([0,0,-radius]) #
        verts.append([0,0, radius]) #
        vi_high  = len(verts)-2
        vi_low = len(verts)-1
        for phi_index in range(nphi): # bottom and top pole triangles
            faces.append([vi_low,   vi(1,        phi_index)  ,  vi(1,        phi_index+1)  ])
            faces.append([vi_high,  vi(ntheta-1, phi_index+1),  vi(ntheta-1, phi_index),   ])


    return Mesh(verts, faces)




def sphere(location, radius, fineness = 2, sides = None):
    mesh = sphere_primitive_polarmesh(radius, length, fineness=fineness, sides=sides)
    # now translate that sphere
    #....
    return mesh
