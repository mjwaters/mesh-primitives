import numpy as np
from stl import mesh

def write_face_vertex_mesh_to_stl(filename, faces, vertices):


    mymesh = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, face in enumerate(faces):
        for j in range(3):
            mymesh.vectors[i][j] = vertices[face[j],:]

    # Write the mesh to file "cube.stl"
    mymesh.save(filename)
