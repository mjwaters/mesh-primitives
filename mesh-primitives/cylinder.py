from mesh import Mesh
import numpy as np

def cylinder_primitive(radius=1, length=1, fineness = 2, sides = None, close_ends = True, use_half_step_twist=True):
    '''fineness should give be used universal refinement of primatives with the mesh'''
    if sides is None:
        assert type(fineness)==int
        assert fineness >= 0
        nsides = 3 * 2**(fineness)
    else:
        assert sides >= 2 # makes a tetrahedron if sides=2
        nsides = sides

    dtheta = 2*np.pi/nsides

    verts = []
    faces = []
    hs = 0
    if use_half_step_twist:
        hs = 1/2

    for ns in range(nsides):
        verts.append([radius*np.cos(      ns*dtheta), radius*np.sin(      ns*dtheta),      0])
        verts.append([radius*np.cos((ns+hs)*dtheta), radius*np.sin((ns+hs)*dtheta), length])
        faces.append([2*ns,   2*ns+1, (2*ns-1)%(2*nsides)])
        #correct order for normals
        faces.append([2*ns+1,   2*ns, (2*ns+2)%(2*nsides)])

    if close_ends:
        verts.append([0,0,0]) # 2*nsides
        verts.append([0,0,length]) # 2*ns+1
        for ns in range(nsides): # bottom cap triangles before top cap
            faces.append([2*ns,   2*nsides, (2*ns+2)%(2*nsides)])
        for ns in range(nsides):
            faces.append([2*ns+1, 2*nsides+1, (2*ns-1)%(2*nsides)])
    return Mesh(verts, faces)



def cylinder(start, end, radius, fineness = 2, sides = None):
    length = np.linalg.norm(end-start)
    mesh = cylinder_primitive(radius, length, fineness=fineness, sides=sides)
    # now rotate and translate that cylinder_primitive
    #....
    return mesh
